package pom;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MiNewestFirstPage extends ProjectMethods {

	public MiNewestFirstPage() {
		
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath="//div[@class='_3wU53n']")
	private List<WebElement> MiAllProducts;
	
	@FindBy(xpath="//div[@class='_1vC4OE _2rQ-NK']")
	private List<WebElement> MiAllProductPrices;
	
	//public static String Firstproducttext="";
	
	public  MiNewestFirstPage getAllMiProductName() {
		
		List<String> AllMiProductName=new ArrayList<String>();
		
		for(int i=0;i<MiAllProducts.size();i++) {
			
			AllMiProductName.add(getText(MiAllProducts.get(i)));
			
		}
		System.out.println("Total no of products "+AllMiProductName.size());
		
		System.out.println("------");
		
		System.out.println(AllMiProductName);
		
		return new MiNewestFirstPage();
	}
	public MiNewestFirstPage getAllMiProductPrices() {
		
		List<String> AllMiProductPrices=new ArrayList<String>();
		
		for(int i=0;i<MiAllProductPrices.size();i++) {
			String price="Rs."+getText(MiAllProductPrices.get(i)).replaceAll("\\D","");
			AllMiProductPrices.add(price);
			
		}
		System.out.println(AllMiProductPrices);
		
		return new MiNewestFirstPage();
		
	}
	
	public FirstProductPage clickonfirstproduct() {
		
		//Firstproducttext=Firstproducttext+getText(MiAllProducts.get(0));
		click(MiAllProducts.get(0));
		return new FirstProductPage();
		
	}
	
	
	
}
