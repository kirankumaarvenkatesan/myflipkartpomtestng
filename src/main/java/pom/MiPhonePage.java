package pom;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MiPhonePage extends ProjectMethods {

	public MiPhonePage() {
		
		PageFactory.initElements(driver,this);
		
	}
	
	@FindBy(xpath="//div[text()='Newest First']")
	private WebElement minewestfirstlink;
	
	public MiNewestFirstPage clickminewestfirstlink() {
		
		click(minewestfirstlink);
		return new MiNewestFirstPage();
	}
	
	public MiPhonePage verifyMiPagetitle(String expectedtitle) {
		
		boolean verifyres=verifyTitle(expectedtitle);
		
		if(verifyres) {
			
			System.out.println("The MiPage title matching");
		}
		else {
			System.out.println("The Mi Page title not matching");
		}
		
		return new MiPhonePage();
		
	}
	
}
