package pom;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods{

	public HomePage() {
		
		PageFactory.initElements(driver,this);
		
	}
	
	@FindBy(xpath="//button[text()='✕']")
	private WebElement closepopupbutton;
	
	@FindBy(xpath="//span[text()='Electronics']")
	private WebElement electronicslink;
	
	@FindBy(xpath="(//a[text()='Mi'])[1]")
	private WebElement Milink;
	
	@FindBy(xpath="(//a[text()='Realme'])[1]")
	private WebElement Realmelink;
	
	public HomePage closepopup() {
		
		click(closepopupbutton);
		
		return new HomePage();
	}
	public HomePage movetoelectronics() {
		
		Hoveronelement(electronicslink);
		return new HomePage();
	}
	public MiPhonePage clickMilink() {
		
		click(Milink);
		
		WebDriverWait wait=new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.titleContains("Mi"));
		
		return new MiPhonePage();
		
	}
	
	public RealmePhonePage clickrealmelink() {
		
		click(Realmelink);
		WebDriverWait wait=new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.titleContains("RealMe"));
		return new RealmePhonePage();
	}
	
	
}
