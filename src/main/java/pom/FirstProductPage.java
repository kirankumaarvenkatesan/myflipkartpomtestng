package pom;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FirstProductPage extends ProjectMethods {
	
	public FirstProductPage() {
		
		PageFactory.initElements(driver, this);
		
	}

   @FindBy(xpath="//span[@class='_38sUEc']//span[contains(text(),'Ratings')]")
   private WebElement Ratings;
   
   @FindBy(xpath="//span[@class='_38sUEc']//span[contains(text(),'Reviews')]")
   private WebElement Reviews;
   
   public FirstProductPage getratingscount() {
	   
	   String ratingcount=getText(Ratings);
	   
	   System.out.println(ratingcount);
	   
	   return new FirstProductPage();
   }
   public FirstProductPage getreviewcount() {
	   
	   String reviewscount=getText(Reviews);
	   
	   System.out.println(reviewscount);
	   return new FirstProductPage();
	   
   }
	
	
	
}
