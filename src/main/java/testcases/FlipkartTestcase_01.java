package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pom.HomePage;
import wdMethods.ProjectMethods;

public class FlipkartTestcase_01 extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC001_Flipkart";
		testDescription = "Flipkart validation";
		authors = "kiran";
		category = "smoke";
		dataSheetName = "TC_001_Mi";
		testNodes = "Leads";
	}
	
	@Test(dataProvider="fetchData")
	public void verifyflipkart(String expectedtitle) {
		
		new HomePage()
		
		.closepopup()
		
		.movetoelectronics()
		
		.clickMilink()
		
		.verifyMiPagetitle(expectedtitle)
		
		.clickminewestfirstlink()
		
		.getAllMiProductName()
		
		.getAllMiProductPrices()
		
		.clickonfirstproduct()
		
		.getratingscount()
		
		.getreviewcount();
		
		
		
		
	}
	
	
}
